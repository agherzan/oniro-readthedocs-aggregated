.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. |main_project_name| replace:: Oniro Project
.. |company_entity| replace:: Oniro Project Working Group
.. |company_entity_shortname| replace:: Oniro Project Working Group
.. |main_policy_url| replace:: https://www.ostc-eu.org/
.. |openchain_specification| replace:: `OpenChain Specification 2.0 <https://wiki.linuxfoundation.org/_media/openchain/openchainspec-2.0.pdf>`__
.. |openchain_specification_license| replace:: `CC-BY-4.0 <https://creativecommons.org/licenses/by/4.0/>`__
.. |email_for_external_inquiries| replace:: davide.ricci@huawei.com
