.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: ../definitions.rst

.. _introduction-and-scope:

Introduction and Scope
######################

Open source compliance is the process by which users, integrators, and developers of open source software – when reusing third parties’ open source software for developing and distributing their own software – ensure to meet and comply with the license conditions imposed by the original copyright holders.

In the context of the |main_project_name| project, main objectives for open source software (OSS) compliance for |company_entity_shortname| are:

-  comply with open source licenses of third party components included in |main_project_name|;
-  license |company_entity_shortname|’s copyrights on |main_project_name| as open source in a correct and unambiguous way;
-  protect |company_entity_shortname|‘s, |company_entity_shortname| customers’ and third parties’ proprietary IP not intended to be released as open source;
-  facilitate the effective use of |main_project_name| by downstream implementers / device makers by providing them with reliable license and copyright metadata and with tools to produce Compliance Artifacts for their products.

For the above purposes, |company_entity_shortname| adopted a general Open Source Policy, addressed both to |company_entity_shortname|’s internal developers and to external contributors to projects related to |main_project_name| ecosystem.

These Open Source Policy Implementation Guidelines, addressed to |company_entity_shortname|’s internal developers only, are aimed at capturing all requirements that need to be implemented in order to have a fully functional Open Source compliance Program within |company_entity| in conformance with the |openchain_specification|.

As the main Open Source Policy, these implementation guidelines cover |main_project_name| and any other open source project hosted and maintained by |company_entity| (|company_entity_shortname|), and related to |main_project_name| ecosystem.
