.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: ../definitions.rst

.. _glossary:

Glossary
########

Inbound, Outbound license
   Respectively, the license used by an upstream project whose software is included in the developed combination, and the license which is used by the project when distributing the software;
Inbound (in)compatibile (license)
   License of A is inbound incompatibile with license of B when license of B does not tolerate including A in an A+B combination licensed under the license of B.
Outbound (in)compatibile (license)
   License of A is outbound incompatibile with license of B when license of A does not tolerate A being included in an A+B combination licensed under the license of B
Derivative
   Derivative is the same concept and shall have the same meaning as in software copyright. For the technical aspects, we mainly refer to the official guidelines and statements of the FSF (particularly, the statements about the ‘(mere) aggregation’ concept that can be found both in the `FAQ on GPLv2 <https://www.gnu.org/licenses/old-licenses/gpl-2.0-faq.en.html#TOCMereAggregation>`__ and in the `FAQ on GPLv3 <https://www.gnu.org/licenses/gpl-faq.en.html#MereAggregation>`__).

Mere aggregation
   A combination of two or more software artifacts (eg. applications, libraries, kernel, etc.) in the same distribution without one being the derivative of the other, despite – in case – one cannot work without the other, but they operate at arm’s length (e.g., through interfaces, sockets, filesystem, database tables).
Own software
   Software whose copyright is owned by the entity distributing or using the same software.
Upstream, downstream
   Direction from and to which software and other information or technology flows in a chain of interaction from the originating artifacts and their modifications until they reach the final distribution outlet. If entity A provides technology to entity B, that receives and transforms it, A i upstream to B and B is downstream to A.
Compliance artifacts
   [from `Openchain 2.0 defintion <https://wiki.linuxfoundation.org/_media/openchain/openchainspec-current.pdf>`__] a collection of artifacts that represent the output of the OpenChain-compliant process for the published software. The collection may include (but is not limited to) one or more of the following: source code, attribution notices, copyright notices, copy of licenses, modification notifications, written offers, Open Source component bill of materials, and SPDX documents.
Open Source License
   In descending order of preference *(a)* an `OSI-approved license <https://opensource.org/licenses/>`__, or *(b)* a license qualifying as free software according to the `list published by the Free Software Foundation (FSF) <https://www.gnu.org/licenses/license-list.html#NonFreeSoftwareLicenses>`__, or *(c)* a license falling within the `Open Source Definition <https://opensource.org/osd>`__ and/or within the `FSF definition of Free Software <https://www.gnu.org/philosophy/free-sw.html>`__, **but** which is neither OSI-approved nor expressly listed by FSF (note that this latter option requires a previous assessment by |legal| to be considered “open source”)
License Proliferation
   Tendency to create an unjustified high number of licenses, often for one single project, causing increased friction, uncertainty, incompatibilities, for no good reason.
