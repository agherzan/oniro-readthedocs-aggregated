.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: ../definitions.rst

.. _creating-new-subprojects:

Creating New (Sub)Projects
##########################

.. _reuse-first-is-there-really-the-need-for-a-new-project:

Reuse first: is there really the need for a new project?
********************************************************

When developing new components and functionalities, before deciding to start a new project developers are encouraged to first consider existing open source alternatives, following the criteria set out in sec. :ref:`7 <sec-incorporating>` .

.. _license-choice:

License choice
**************

If no viable existing solution is found, the project team will have to clarify from the very beginning the applicable license for the new project which should be approved by |legal| and |ip_expert|.

Any |main_project_name|-related project should be open source by default.

As a general rule, |main_project_name|-related projects should default to |main_project_license|, unless there is a proven and justified reason, discussed with |legal|. There may be cases in which we must take into account licensing preferences and expectations of a project’s target contributor and user community, and possible legal constraints (eg. as a general rule, Linux kernel modules should be licensed under GPLv2 because that is the license of the Linux Kernel and it is a strong copyleft license).

In any case, the chosen license must be an Open Source License according to the definition given in :ref:`Glossary <glossary>` , taking into consideration the guiding principle so as to avoid as much as possible license proliferation as defined in :ref:`Glossary <glossary>` , by using only well-known open source licenses, and by avoiding creating new open source licenses or new license exceptions at all.

.. _codes-of-conduct:

Codes of Conduct
****************

We believe that any contributor (internal or external) to |main_project_name| projects should have the right and duty to interact in ways that contribute to an open, welcoming, diverse, inclusive, and healthy community.

As a means to achieve this, including a code of conduct along with a new project’s license is mandatory.

As a general indication, we suggest |company_entity_shortname| developers to use the `Contributor Covenant 2.0 <https://www.contributor-covenant.org/version/2/0/code_of_conduct/>`__).
