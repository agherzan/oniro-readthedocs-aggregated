.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: ../definitions.rst

.. _governance:

Governance
##########

|company_entity_shortname| commits to design and implement good governance processes to ensure that the principles and values expressed in this policy are followed over time within our organization.

All governance processes will be designed having in mind the principles set out in sec. :ref:`5.3 <sec-meritocracy>` . It is understood that OSS compliance is a continuous process, that in the context of |main_project_name| needs to be run in parallel with the development process.

To achieve this, we commit to avoid over-engineering of governance processes, and to make use of ticketing systems, CI/CD facilities and DevOps-style workflows as much as possible, whilst giving developers access to internal legal and other OSS-related organizational resources.

|company_entity_shortname| developers and decision-makers shall adhere to OSS Policy Implementation Guidelines that are internally published from time to time, and use software tools and technical facilities provided by those guidelines when working on projects related to |main_project_name|.

To this purpose, |company_entity_shortname| developers and decision-makers will be provided with continuous training programs on the content of this policy and the related implementation guidelines.
