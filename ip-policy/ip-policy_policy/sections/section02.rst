.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: ../definitions.rst

.. _scope:

Scope
#####

.. _objective-scope:

Objective Scope
***************

This policy covers |main_project_name_bold| and any other open source project related to |main_project_name| ecosystem, hosted and maintained by |company_entity| (|company_entity_shortname_bold|).

.. _subjective-scope:

Subjective Scope
****************

This policy is binding for |company_entity_shortname|’s developers, collaborators and other individuals or entities connected to the development of software in or for |company_entity_shortname|. External contributors and community members shall undertake and fully commit to follow its guidelines and principles (and particularly those set out in sec. :ref:`4.2 <sec-upstreamfirst_3rdparty>`  and in sec. :ref:`5.2 <sec-how_others_can_collaborate>` ) when they are collaborating to projects related to |main_project_name|.

.. _entry-into-force:

Entry Into Force
****************

This policy is first published on |policy_publication_date|.

Implementation of this policy within |company_entity_shortname|’s organization will require a preliminary stage involving the allocation of internal resources with the appointment of the required roles, the adjustment of internal procedures and workflows, the set-up and testing of compliance toolchains, the training of the affected staff members.

After completion of such preliminary stage, this policy will definitively come into force on |policy_entry_into_force_date|.
