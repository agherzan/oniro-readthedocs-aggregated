.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: ../definitions.rst

.. _collaborating:

Collaborating
#############

.. _sec-OSS_is_collaboration:

Open Source is about Collaboration
**********************************

Open source, it goes without saying, is about collaboration. |company_entity_shortname| is committed to being available as much as possible and practicable in making their open source software truly accessible and easily reusable by anybody in the downstream part of the supply chain.

At the same time, open source collaboration is unique in that the licensing *per se* is sufficient for a project to be enabled to *receive* contributions from the downstream, and any entity in the ecosystem can be both upstream and downstream at the same time without the need to have any interaction with the project other than at commit time. The only necessary interface to give and receive software contributions both upstream and downstream is the license.

`OpenChain <https://openchainproject.org/>`__ facilitates the interaction between organizations. It does so by internally guiding |company_entity_shortname| deploying workable rules, principles, knowledge. it also provides evidence and interfaces to interact with downstream and upstream entities for any activity involving the software. |openchain_conformance_timing|

Some projects, especially corporation-controlled or sponsored, have developed policies requiring that all the software be centralized with only one copyright holder, either by appointing it the full owner or just a fiduciary owner on behalf of the community. This generally requires signing a legal deed, often referred to as CLA (short for “Contribution Licensing Agreement”) or “CAA” (“Contribution *Assignment* Agreement”). Other projects prefer a more lightweight approach, and only require a general reassurance that the contributor is identified and accepts the terms of licensing (it is the case of DCOs).

.. _sec-how_others_can_collaborate:

How others can collaborate in our projects
******************************************

.. _dco:

DCO
===

All contributions must be signed off by the contributor in each commit with a ``signed-off-by:`` and the name of the author of the commit. |company_entity_shortname| only accepts **pseudonyms** in case the author of the commit is well-known under that name. In case of doubt, the authors must use their real name and an email address that they control.

Signing off the commit means that the author commits and declares what is stated in the Linux Foundation’s `Developer’s Certificate of Origin <https://developercertificate.org/>`__ (or “DCO”). Each repository must have a ``contributing.md`` file that reflects this policy. All contributions that do not contain a sign-off statement will be rejected or will receive a request to sign-off.

.. _sec-meritocracy:

Meritocracy
***********

All contributions to projects that |company_entity_shortname| stewards ‒ or in which it has directional power ‒ follow a principle of meritocracy and non discrimination. This means that contributions will, as much as possible, be dealt with as equal, irrespective of who has contributed them and decisions or votes shall be taken in the most dispassionate way. |company_entity_shortname| places a high value and consideration on the need to be impartial when deciding to be involved in a third party’s project, and therefore adheres to the same principles itself.

|company_entity_shortname| considers the `Open Decision Framework <https://github.com/red-hat-people-team/open-decision-framework>`__ a good reference for the above process. Other guidance documents may be adopted on a case-by-case basis to better fit in the organizational and cultural environment where |company_entity_shortname| operates.
